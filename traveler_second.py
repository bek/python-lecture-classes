class Traveler:
    """Traveler class represents somenone who journeys"""

    def __init__(self):
        self.countries_visited = 0

    def describe(self):
        # Print a statement about the traveler's experience
        print(f"Traveler has visited {self.countries_visited} countries so far")

    def update_countries(self, num):
        if num < 193:
            self.countries_visited = num
        else:
            raise Exception("The planet Earth has only 192 countries")


traveler = Traveler()
traveler.describe()
# Traveler has visited 0 countries so far

traveler.update_countries(5)
traveler.describe()
# Traveler has visited 5 countries so far

try:
    traveler.update_countries(251)
except Exception as e:
    print(f"Some error happened, but wanna see the rest of the program")
    print(f"this is the error message: {e}")
traveler.describe()
# Some error happened, but wanna see the rest of the program
# this is the error message The planet Earth has only 192 countries
# Traveler has visited 5 countries so far
