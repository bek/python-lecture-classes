from person import Person

data = [
    ("Ulugbek Baymuradov", 42, "Tashkent, Uzbekistan", 3),
    ("Jake Russo", 22, "Ithaca, New York", 55),
    ("Sylvia Osborne", 44, "Bali, Indonesia", 9),
]


class Traveler(Person):
    def __init__(self, name, age, address, num_countries_traveled):
        super().__init__(name, age, address)
        self.update_countries(num_countries_traveled)

    def describe(self):
        # Print a statement about the traveler's experience
        self.full_bio()
        print(f"and has visited {self.countries_visited} countries so far")

    def update_countries(self, num):
        if num < 193:
            self.countries_visited = num
        else:
            raise Exception("The planet Earth has only 192 countries")

    def get_name(self):
        print(f"{self.name} the traveler")


# traveler_instance = Traveler(*data[1])
# traveler_instance.get_name()
# Jake Russo the traveler

# traveler_instance = Traveler(*data[0])
# traveler_instance.describe()
# Ulugbek Baymuradov is 42 years old and lives in Tashkent, Uzbekistan
# and has visited 3 countries so far
