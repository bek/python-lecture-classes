class Traveler:
    """Traveler class represents somenone who journeys"""

    def __init__(self):
        self.countries_visited = 0

    def describe(self):
        # Print a statement about the traveler's experience
        print(f"Traveler has visited {self.countries_visited} countries so far")


traveler = Traveler()
traveler.describe()
# Traveler has visited 0 countries so far

traveler.countries_visited = 5
traveler.describe()
# Traveler has visited 5 countries so far

traveler.countries_visited = 251
traveler.describe()
# Traveler has visited 251 countries so far
