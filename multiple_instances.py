from person import Person

data = [
    ("Ulugbek Baymuradov", 42, "Karshi, Uzbekistan"),
    ("Jake Russo", 22, "Ithaca, New York"),
    ("Sylvia Osborne", 44, "Bali, Indonesia"),
]

friends = []

for entry in data:
    name, age, address = entry
    person_instance = Person(name, age, address)
    friends.append(person_instance)

for person in friends:
    person.full_bio()
