import requests
import configparser

config = configparser.ConfigParser()
config.read("config.ini")


class Address:
    def __init__(self, address_string):
        address_words = address_string.split(",")
        self.city = address_words[0]
        self.country = address_words[1]
        self.get_climate_data()

    def full_address(self):
        return self.city + "," + self.country

    def get_climate_data(self):
        url = "https://api.waqi.info/feed/CITY/?token=SECRET_KEY".replace(
            "CITY", self.city
        ).replace("SECRET_KEY", config["aqicn.org"]["token"])
        response = requests.get(url)
        self.climate = response.json()

    def temperature(self):
        return self.climate.get("data", {}).get("iaqi", {}).get("t", {}).get("v", None)

    def air_quality_index(self):
        return (
            self.climate.get("data", {}).get("iaqi", {}).get("pm25", {}).get("v", None)
        )

    def describe(self):
        print(
            "In the city of {}, temperature is {} C degrees".format(
                self.city, self.temperature()
            )
        )
        print(f"and air quality is {self.air_quality_index()}")
