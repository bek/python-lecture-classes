class Person:
    """A base Person class to model a person"""

    def __init__(self, name, age, address):
        """Initializes attributes"""
        self.name = name
        self.age = age
        self.address = address

    def get_name(self):
        print(f"{self.name}")

    def get_age(self):
        print(f"{self.age}")

    def full_bio(self):
        print(f"{self.name} is {self.age} years old and lives in {self.address}")
