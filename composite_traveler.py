from inheritance import Traveler
from address import Address

data = [
    ("Ulugbek Baymuradov", 42, "Tashkent, Uzbekistan", 3),
    ("Jake Russo", 22, "Ithaca, New York", 55),
    ("Sylvia Osborne", 44, "Bali, Indonesia", 9),
]


class InformedTraveler(Traveler):
    def __init__(self, name, age, address, num_countries_traveled):
        super().__init__(name, age, address, num_countries_traveled)
        self.address = Address(self.address)

    def full_bio(self):
        print(
            "{} is {} years old and lives in {},{}".format(
                self.name, self.age, self.address.city, self.address.country
            )
        )

    def describe(self):
        super().describe()
        self.address.describe()


advanced_traveler = InformedTraveler(*data[0])
advanced_traveler.describe()
# Ulugbek Baymuradov is 42 years old and lives in Tashkent, Uzbekistan
# and has visited 3 countries so far
# In the city of Tashkent, temperature is 14 C degrees
# and air quality is 172
