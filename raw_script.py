# coding: utf-8

# In[1]:


import pyBigWig
import numpy as np
from tensorflow.keras.layers import Conv1D, Dense, MaxPooling1D, Flatten
from tensorflow.keras.models import Sequential
from keras import backend as K


# In[2]:


def earth_mover_distance_loss(y_true, y_pred):
    return K.mean(y_true * y_pred)


# In[3]:


bw_input = pyBigWig.open(
    "/data/GM12878/SRR1552485.fastqbowtie2.sorted.pos.BedGraph.sort.bedGraph.bw"
)


# In[4]:


bw_negative_input = pyBigWig.open(
    "/data/GM12878/SRR1552485.fastqbowtie2.sorted.neg.BedGraph.sort.bedGraph.bw"
)


# In[5]:


bw_output = pyBigWig.open("/data/GM12878/ENCFF154XCY.bigWig")


# In[6]:


chromosome = "chr1"


# In[7]:


bp_per_bin = 50


# In[8]:


number_of_bins_input = int(bw_input.chroms()[chromosome] / bp_per_bin)


# In[9]:


inputs_raw = np.nan_to_num(
    np.array(bw_input.stats(chromosome, nBins=number_of_bins_input), dtype=float)
)


# In[10]:


negative_inputs_raw = np.nan_to_num(
    np.array(
        bw_negative_input.stats(chromosome, nBins=number_of_bins_input), dtype=float
    )
)


# In[11]:


number_of_bins_output = int(bw_output.chroms()[chromosome] / bp_per_bin)


# In[12]:


outputs_raw = np.nan_to_num(
    np.array(bw_output.stats(chromosome, nBins=number_of_bins_output), dtype=float)
)


# In[13]:


from sklearn.preprocessing import MinMaxScaler

scaler = MinMaxScaler(feature_range=(0, 1))
positive_inputs_normalized = scaler.fit_transform(inputs_raw.reshape(-1, 1))
negative_inputs_normalized = scaler.fit_transform(
    negative_inputs_raw.reshape(-1, 1) * -1
)
outputs_normalized = scaler.fit_transform(outputs_raw.reshape(-1, 1))


# In[14]:


input_vectors = np.stack(
    (positive_inputs_normalized, negative_inputs_normalized), -1
).reshape(-1, 2, 1)
input_vectors[0]


# In[15]:


import matplotlib.pyplot as plt

# plt.plot(outputs_normalized[1000000:1009000])
plt.plot(negative_inputs_normalized)
plt.plot(positive_inputs_normalized)
plt.show()


# In[16]:


gene_region_size = 500  # x 50bp


# In[17]:


inputs = input_vectors[: (-(len(input_vectors) % gene_region_size))].reshape(
    -1, gene_region_size, 2
)
outputs = outputs_normalized[: (-(len(outputs_normalized) % gene_region_size))].reshape(
    -1, gene_region_size
)
print(inputs.shape)
print(outputs.shape)
inputs[0][0]


# In[18]:


from sklearn.model_selection import train_test_split

train_features, test_features, train_labels, test_labels = train_test_split(
    inputs, outputs, test_size=0.25, random_state=42
)


# In[19]:


from tensorflow.keras.layers import (
    Conv1D,
    Conv2D,
    Conv3D,
    Dense,
    MaxPooling1D,
    MaxPooling2D,
    MaxPooling3D,
    Flatten,
    Dropout,
)
from tensorflow.keras.models import Sequential
from keras.utils import multi_gpu_model


# In[247]:


model = Sequential()


# In[248]:


model.add(
    Conv1D(
        filters=int(gene_region_size / 2),
        kernel_size=3,
        activation="relu",
        input_shape=(gene_region_size, 2),
    )
)


# In[249]:


model.add(MaxPooling1D(pool_size=2))
model.add(Dropout(0.25))


# In[250]:


model.add(Conv1D(filters=int(gene_region_size / 4), kernel_size=3, activation="relu"))


# In[251]:


model.add(MaxPooling1D(pool_size=2))
model.add(Dropout(0.25))


# In[252]:


model.add(
    Conv1D(
        filters=int(gene_region_size / 8),
        kernel_size=3,
        activation="relu",
        dilation_rate=2,
    )
)


# In[253]:


model.add(MaxPooling1D(pool_size=2))
model.add(Dropout(0.25))


# In[254]:


model.add(
    Conv1D(
        filters=int(gene_region_size / 20),
        kernel_size=3,
        activation="relu",
        dilation_rate=4,
    )
)


# In[255]:


model.add(Flatten())


# In[256]:


model.add(Dense(gene_region_size, activation="relu"))


# In[257]:


model.compile(
    loss=["mean_squared_error"],
    optimizer="adam",
    metrics=["mean_squared_error", earth_mover_distance_loss],
)


# In[258]:


model.summary()


# In[259]:


history = model.fit(
    train_features,
    train_labels,
    epochs=25,
    verbose=0,
    validation_split=0.25,
    batch_size=32,
)


# In[260]:


import matplotlib.pyplot as plt

plt.figure()
plt.plot(history.history["loss"])
plt.plot(history.history["val_loss"])
plt.title("model loss")
plt.ylabel("loss")
plt.xlabel("epoch")
plt.legend(["train", "validation"])
plt.show()


# In[261]:


prediction = model.predict(inputs)


# In[262]:


prediction = prediction.reshape(1, -1)


# In[239]:


plt.figure()
plt.plot(outputs.reshape(1, -1)[0][1000000:1090000])
plt.plot(prediction[0][1000000:1090000])
plt.show()
